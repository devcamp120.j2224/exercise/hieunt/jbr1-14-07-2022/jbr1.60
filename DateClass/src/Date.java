public class Date {
    int day;
    int month;
    int year;
    // khởi tạo đầy đủ tham số
    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    // getter setter
    public int getDay() {
        return day;
    }
    public int getMonth() {
        return month;
    }
    public int getYear() {
        return year;
    }
    public void setDay(int day) {
        this.day = day;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public void setYear(int year) {
        this.year = year;
    }
    // method set cả 3 tham số
    public void setDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    @Override
    public String toString() {
        return String.format("%02d", day)
        + "/" + String.format("%02d", month)
        + "/" + String.format("%04d", year);
    }
}
